# Ir Forum Blacklist

A simple script for iRacing forum that hides threads created by some users that you want to ignore. Ignore button works but you can still see his threads and I know you'd love having a hide option.

It's so simple because I wanted to create something fast and to be honest I think I won't update it in a long time. I know there is room for improvement but meh, it works.

## Install

Before you can use this script, you need to install a plugin in your favourite browser called [TamperMonkey](https://tampermonkey.net). Then you only have to click
[here](https://bitbucket.org/FranRomero93/ir-forum-blacklist/raw/ef6391e4d8888f0fb6febfec9a25fce2e9dd6f68/iracing-forum-blacklist.user.js) and confirm that you want to install it. If you don't trust me, you can check it before install.
 
## FAQ

### 1. How do I add a guy to the blacklist?

Click Tampermonkey icon > dashboard and open the script. The code is very short so you'll see a variable named __blacklist__. For example, if you don't want to see threads from Jon Snow and Daenerys of the Storm, your blacklist variable should be:

```javascript
let blacklist = ["Jon Snow", "Daenerys of the Storm"];
```

Order doesn't matter but you have to put the name right, spaces and numbers included. Don't forget to save the changes before you close it.

Keep in mind that every time that this script is updated, you have to add the names.


### 2. Could I hide off-topic threads only?.

Of course!. Click Tampermonkey icon > dashboard and open the script. Change match line(top of the file) from:

``
*://members.iracing.com/jforum/forums/show/*
``

To:

``
*://members.iracing.com/jforum/forums/show/606.*
``

If this script is updated, you have to modify this line again.


### 3. I want to donate...

You don't have to, it's free and the code is available. If you still want to donate, you can send me some ir credits.
