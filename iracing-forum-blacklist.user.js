// ==UserScript==
// @name         iR Forum Blacklist
// @namespace    https://bitbucket.org/FranRomero93/ir-forum-blacklist/src/master/
// @version      0.1
// @description  A simple script for iRacing forum that hides threads created by some users that you want to ignore.
// @author       Francisco J Romero
// @licence      MIT
// @match        *://members.iracing.com/jforum/forums/show/*
// @grant        none
// @run-at       document-end
// ==/UserScript==

(function() {
    'use strict';

    let blacklist = [];

    $('#forum > tbody > tr').each(function(index) {
        let author = $(this).find('.tdAuthor').text();

        if ($.inArray(author, blacklist) !== -1) {
            this.remove();
        }
    });
})();